/*
Copyright 2017 Tiago Cogumbreiro

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <vector>
#include <stack>
#include <memory>

// A prefix is used to compare the happens-before relation between two vectors.
// A prefix is the first index in two vectos where the values differ.
// That is, prefix k in vectors x and y is such that if i < k,
// then x[i] == y[i].
struct Prefix {
    // The index
    int prefix;
    // a pointer to the left and right spawn-paths
    const std::vector<int> &left;
    const std::vector<int> &right;
    // Given two vectors we find the first different index.
    inline Prefix(const std::vector<int> &left, const std::vector<int> &right)
        : prefix(0), left(left), right(right) {
        // finds the first index that differs
		int minLen = std::min(left.size(), right.size());
		for (prefix = 0; prefix < minLen && left[prefix] == right[prefix]; prefix++) {
			// nothing to do
		}
    }
    // Two vectos are the same when they have the same length and the
    // prefix is the length.
    inline bool is_equal() {
        return prefix == left.size()
            && prefix == right.size();
    }
    // check the private section
    inline bool happens_before() { return happens_before(left, right); }

    // Two vectos 
    inline bool is_par() {
        return !(is_equal()
            || happens_before(left, right)
            || happens_before(right, left));
    }

    // Returns true if, and only if, the left task is a parent of the right
    // task.
    inline bool is_parent_of() {
		return left.size() + 1 == right.size()
		    && prefix >= left.size() - 1;
	}

private:
    // l happens before right if: 
    inline bool happens_before(const std::vector<int> &l, const std::vector<int> r) {
	    return l.size() <= r.size() // (i) left is not a bigger vector
	        && prefix < r.size()    // (ii) prefix is an index of right
	        && (prefix == l.size()  // (iii.a) left is a direct child of right
	                                // (iii.b) left was spawned before
	            || (prefix + 1 == l.size() && l[prefix] < r[prefix]));
    }
    
};

// Represents the task-id and the logical time of a given task. Specifically,
// semantically a spawn-path object is the node's position within a spawn path.
struct SpawnPath {
    // the first path is a one-sized vector
    SpawnPath() : path({0}) {}

    // Produces an event.
    // The current task is always the last component of a vector.
    // Thus, we advance (increment) the logical time of the current task.
    // Creates a copy with the effect of the operation.
    inline SpawnPath event() const {
        auto newPath = path;
        newPath.back() += 1;
        return SpawnPath(std::move(newPath));
    }

    // Creating a child is adding a new element to the left of the vector.
    // After spawning the parent must invoke `event()`.
    // Returns a copy with the effect of the operation.
    inline SpawnPath spawn() const {
        auto newPath = path;
        newPath.push_back(0);
        return SpawnPath(std::move(newPath));
    }

    // Happens-before or equals the other path.
    // See `Prefix::happens_before()` and `Prefix::equals()`.
    inline bool happens_before_eq(const SpawnPath &other) const {
        Prefix result(path, other.path);
        return result.happens_before() || result.is_equal();
    }

    // Returns when both tasks may happen in parallel.
    // See also `Prefix::is_par`.
    inline bool is_par(const SpawnPath &other) const {
        Prefix result(path, other.path);
        return result.is_par();
    }

    // Checks if the target task is a parent of the current task.
    inline bool is_parent_of(const SpawnPath &other) const {
        Prefix result(path, other.path);
        return result.is_parent_of();
    }

private:
    const std::vector<int> path;
    // Creates a spawn-path from a vector.
    SpawnPath(std::vector<int> path) : path(path) {}
};

// Read-only and thread-safe linked list.
template<typename T>
struct Link {
    const std::shared_ptr<T> element;
    const std::shared_ptr<Link<T>> next;

    Link(std::shared_ptr<T> elem):element(elem) {}
    Link(std::shared_ptr<T> elem, std::shared_ptr<Link<T>> next)
        : element(elem), next(next) {}
};

// A (compressed) graph to represent the happens-before relation.
struct JoinGraph {
    // Creates the root node.
    JoinGraph() : value() {}

    // Creates a new node. Connects this node to the target node.
    // Returns the new node. Should be invoked after spawning to return
    // the continuation node.
    std::shared_ptr<JoinGraph> event() const {
        return std::make_shared<JoinGraph>(std::move(value.event()), joins);
    }

    // Creates a new node. Connects this node to the target node.
    // Returns the new node.
    std::shared_ptr<JoinGraph> spawn() const {
        return std::make_shared<JoinGraph>(std::move(value.spawn()), joins);
    }

    // Creates a new node. Connects current node to the new node; connects
    // the target node to the new node.
    std::shared_ptr<JoinGraph> join(std::shared_ptr<JoinGraph> other) const {
        auto newJoins = std::make_shared<Link<JoinGraph>>(std::move(other), joins);
        return std::make_shared<JoinGraph>(value.event(), newJoins);
    }

    // Checks if this node happens before than target node.
    bool happens_before_eq(std::shared_ptr<JoinGraph> other) const {
        return JoinGraph::happens_before_eq(value, *other);
    }
    
    JoinGraph(SpawnPath value, std::shared_ptr<Link<JoinGraph>> joins)
        : value(std::move(value)), joins(std::move(joins)) {}
private:
    const SpawnPath value;
    const std::shared_ptr<Link<JoinGraph>> joins;

    // A node can only happen before another node in the graph if
    // the spawn path of left is in the joins of the node of right.
    static bool happens_before_eq(const SpawnPath &left, const JoinGraph &right) {
        // The spawn-paths happen before, then we are done.
        if (left.happens_before_eq(right.value)) {
            return true;
        }
        // No joins were performed, so we are done, no need to allocate a stack.
        if (right.joins == nullptr) {
            return false;
        }
        // Create a stack to keep track of visited nodes.
        std::stack<std::shared_ptr<JoinGraph>> proc;
        // Add all joined nodes to the stack.
        for(auto elem = right.joins; elem != nullptr; elem = elem->next) {
            proc.push(elem->element);
        }
        // Process all nodes to be visited
        while (proc.size() > 0) {
            auto elem = proc.top();
            proc.pop();
            // If the left path happens before this node, then we are done.
            if (left.happens_before_eq(elem->value)) {
                return true;
            }
            // otherwise, continue.
            for(auto it = elem->joins; it != nullptr; it = it->next) {
                proc.push(it->element);
            }
        }
        // the left path was not in the graph in the right
        return false;
    }
};
